//
//  MasterViewController.m
//  EventLog
//
//  Created by Mac on 11.11.13.
//  Copyright (c) 2013 maximusss. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "AppDelegate.h"
#import "Event.h"
#import "EventConstant.h"
#import "NetworkManager.h"
#import "AFNetworking.h"

@interface MasterViewController () {
    NSInteger row;
}
@end

@implementation MasterViewController


@synthesize eventArray=_eventArray, addButton=_addButton;

- (void)awakeFromNib
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad )
    {
        UIBarButtonItem *add = [[UIBarButtonItem alloc] initWithTitle:@"Create" style:
                                       UIBarButtonItemStylePlain target:self action:@selector(addButtonClicked:)];
        self.navigationItem.rightBarButtonItem = add;
    } else
    {
        self.navigationItem.rightBarButtonItem = self.addButton;
    }
  
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:@"refreshTableView" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_eventArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath    //инициализация
                                                                                                          //ячеек
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if ( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"] ;
    }
    
    Event *cellEvent = [self.eventArray objectAtIndex:indexPath.row];
    cell.textLabel.text = cellEvent.title;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MM/dd/yyyy"];
    
    NSString *date = [format stringFromDate:cellEvent.date];
    
    [format setDateFormat:@"h:mm a"];
    
    NSString *time = [format stringFromDate:cellEvent.time];
    
    NSString *dateTime = [NSString stringWithFormat:@"%@ - %@", date, time];
    
    cell.detailTextLabel.text = dateTime;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    BOOL value = [cellEvent.impotance boolValue];                   //проверка на важность события
    if ( value == YES ) {
        cell.backgroundColor = [UIColor cyanColor];
    } else {
        cell.backgroundColor = [UIColor clearColor];
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];            //удаление данных ячейки
        NSManagedObjectContext *context = [appDelegate managedObjectContext];               //из Core Data
        
        Event *deletedEvent = [self.eventArray objectAtIndex: indexPath.row];
        if ( deletedEvent == self.detailViewController.event ) {
            [self.detailViewController eventChanged:nil];
        }
        
        
        [self deleteEventInURL:[deletedEvent valueForKey:TITLE]];

        
        [context deleteObject:deletedEvent];
        [appDelegate saveContext];
        
        [self.eventArray removeObjectAtIndex:indexPath.row];                                //стирание ячейки из таблицы
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                              withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView reloadData];
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)       //передача данных в iPad
    {
        self.detailViewController->deleted = YES;                                      //в детализированное представление
        self.detailViewController->modal = NO;
        Event *event = self.eventArray[indexPath.row];
        row = indexPath.row;
        [self.detailViewController eventChanged:event];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"])                       //показ и редактирование события
    {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Event *newEvent = self.eventArray[indexPath.row];
        DetailViewController *detVC =[segue destinationViewController];
        detVC.event = newEvent;
        detVC->deleted = YES;
        detVC->modal = NO;
    } else if ([[segue identifier] isEqualToString:@"addEvent"])                   //добавление нового события
    {
      
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self refreshData];
}

-(IBAction)addButtonClicked:(id)sender                          //добавление события на iPad
{
    self.detailViewController->deleted = NO;
    self.detailViewController->modal = YES;
    [self.detailViewController eventChanged:nil];
}

-(void)refreshData
{
    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [ appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event"
                                inManagedObjectContext:managedObjectContext];
    [request setEntity:entity];

    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
    [request setSortDescriptors:[NSArray arrayWithObject:sortDescriptor ]];
    
    NSError *error;
    NSMutableArray *mutableFetchResults = [[managedObjectContext executeFetchRequest:request
                                                                               error:&error] mutableCopy];
    if ( mutableFetchResults == nil ) {
        //Mistake solve
    }
    
    self.eventArray=mutableFetchResults;
    Event *event = self.eventArray[row];
    [self.detailViewController eventChanged:event];
    
    [self.tableView reloadData];
}

-(void)deleteEventInURL:(NSString *) eventIDTitle
{
    [[NetworkManager instance] GET:@"find/EventLogUser" parameters:nil
                           success:^(NSURLSessionDataTask * __unused task, id JSON) {
                               
                               NSArray *key = [JSON allKeys];
                               NSArray *object = [JSON allObjects];
                               
                               for (NSUInteger i=0; i<[key count]; i++)
                               {
                                   NSDictionary *dict = [object objectAtIndex:i];
                                   NSString *title = [dict valueForKey:TITLE];
                                   
                                   if ([title isEqualToString:eventIDTitle])
                                   {
                                       NSString *deletedID =[[key objectAtIndex:i] description];
                                       
                                       [[NetworkManager instance] GET:[@"delete/" stringByAppendingString:deletedID] parameters:nil
                                                              success:^(NSURLSessionDataTask * __unused task, id JSON) {
                                                                  
                                                                  NSLog(@"Delete success %@",[JSON description]);
                                                                  
                                                              }
                                                              failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
                                                                  UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Delete event error!"
                                                                                                                  message:[error description] delegate:nil
                                                                                                        cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                                  [alert show];
                                                              }];
                                       
                                   }
                               }
                               
                           }
                           failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
                               UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Find event error!"
                                                                               message:[error description]
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"OK"
                                                                     otherButtonTitles:nil];
                               [alert show];
                           }];
}

@end
