//
//  main.m
//  EventLog
//
//  Created by Mac on 11.11.13.
//  Copyright (c) 2013 maximusss. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}