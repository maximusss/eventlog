//
//  MasterViewController.h
//  EventLog
//
//  Created by Mac on 11.11.13.
//  Copyright (c) 2013 maximusss. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

#import <CoreData/CoreData.h>

@interface MasterViewController : UITableViewController <UITableViewDelegate> {
    NSMutableArray* _eventArray;
    UIBarButtonItem* _addButton;
}

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (nonatomic, retain) NSMutableArray *eventArray;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *addButton;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

-(IBAction)addButtonClicked:(id)sender;
-(void)refreshData;
-(void)deleteEventInURL:(NSString *) eventIDTitle;
@end
