//
//  DetailViewController.m
//  EventLog
//
//  Created by Mac on 11.11.13.
//  Copyright (c) 2013 maximusss. All rights reserved.
//

#import "DetailViewController.h"
#import "AppDelegate.h"
#import "EventConstant.h"

#import "NetworkManager.h"
#import "AFNetworking.h"

@interface DetailViewController () {
    UITextField* activeField;
   
}

- (void)configureView;
@end

@implementation DetailViewController

@synthesize titleTextField=_titleTextField, dateTextField=_dateTextField,
    timeTextField=_timeTextField, recallTextField=_recallTextField,
    descTextField=_descTextField, impotanceSegment=_impotanceSegment;
@synthesize scrollView=_scrollView, contentView=_contentView;
@synthesize event=_event, eventDate=_eventDate;
@synthesize masterPopoverController=_masterPopoverController;
@synthesize datePickerPopover;



#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }

    
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.event.title) {
        self.detailDescriptionLabel.text = [self.event.title description];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
 
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(save:)];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad ) {
        self.scrollView.frame = CGRectMake(0, 0, 1064, 1064);
    } else {
        self.scrollView.frame = CGRectMake(0, 0, 320, 1064);
    }
    [self.scrollView setContentSize:CGSizeMake(320, 1064)];
    
    if ( self->modal ) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    }
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
       //Dismiss keyboard
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
            action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    [self configureView];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -
#pragma Save and cancel

-(IBAction) save:(id)sender {
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSDate *dateRecalling = [dateFormat dateFromString:self.dateTextField.text];
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"h:mm a"];
    NSDate *timeRecalling = [timeFormat dateFromString:self.recallTextField.text];
   
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *timeComponents = [gregorian components:
                                        (NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:timeRecalling];
    NSInteger seconds = timeComponents.hour*3600 + timeComponents.minute*60;
    
    dateRecalling = [NSDate dateWithTimeInterval:seconds sinceDate:dateRecalling];

    if ( [ dateRecalling compare:[NSDate date] ] == NSOrderedAscending )
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"SAVE ERROR!" message:@"The stored event has happened.\nCheck the correct of selected date." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];

        [alertView show];
        
    } else {
       
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        
        NSMutableDictionary* params = [NSMutableDictionary new];
        [params setObject:@"EventLogUser" forKey:@"user"];
        NSString *deletedEventTitle = [self.event valueForKey:TITLE];
        
        if (self->deleted)
        {
            [self deleteEventInURL:deletedEventTitle];
            [context deleteObject:self.event];
            self->deleted=NO;
        }
        
        NSManagedObject *newEvent = [NSEntityDescription insertNewObjectForEntityForName:@"Event"
                                                                  inManagedObjectContext:context];
        
        [newEvent setValue:self.titleTextField.text forKey:TITLE];
        [params setObject:self.titleTextField.text forKey:TITLE];
        
        NSDate *date = [dateFormat dateFromString:self.dateTextField.text];
        [newEvent setValue:date forKey:DATE];
        [params setObject:self.dateTextField.text forKey:DATE];
        
        NSDate *time = [timeFormat dateFromString:self.timeTextField.text];
        [newEvent setValue:time forKey:TIME];
        [params setObject:self.timeTextField.text forKey:TIME];
        
        time = [timeFormat dateFromString:self.recallTextField.text];
        [newEvent setValue:time forKey:RECALL];
        [params setObject:self.recallTextField.text forKey:RECALL];
        
        [newEvent setValue:self.descTextField.text forKey:DESCRIPT];
        [params setObject:self.descTextField.text forKey:@"description"];
        
        if ( self.impotanceSegment.selectedSegmentIndex == YES) {
            [newEvent setValue:@YES forKey:IMPOTANCE];
            [params setObject:[NSString stringWithFormat:@"%@",@YES] forKey:@"importance"];
        } else {
            [newEvent setValue:@NO forKey:IMPOTANCE];
            [params setObject:[NSString stringWithFormat:@"%@",@NO] forKey:@"importance"];
        }
        
        [[NetworkManager instance] POST:@"add" parameters:params
                                success:^(NSURLSessionDataTask * __unused task, id JSON) {
                                    NSLog(@"Success adding: %@",[JSON description]);
                                }
                                failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
                                    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Adding event error!"
                                                                                    message:[error description]
                                                                                   delegate:nil
                                                                          cancelButtonTitle:@"OK"
                                                                          otherButtonTitles:nil];
                                    [alert show];
                                }];
        
        [appDelegate saveContext];
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];

        // Set the fire date/time
        localNotification.fireDate = dateRecalling;
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        
        localNotification.applicationIconBadgeNumber=1;
    
        // Setup alert notification
        localNotification.alertAction = @"Detail";
  
        localNotification.alertBody = self.titleTextField.text;

        localNotification.soundName = UILocalNotificationDefaultSoundName;

        NSDictionary *customInfo = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:
                                                                    self.titleTextField.text,
                                                                    self.timeTextField.text,
                                                                    self.descTextField.text, nil]
                                                               forKeys:[NSArray arrayWithObjects:
                                                                    @"title",
                                                                    @"time",
                                                                    @"desc",nil]];
        localNotification.userInfo = customInfo;

        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    
        if (self->modal) {
        
            UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"ADD NEW EVENT"
                                                                   message:@"New event was created successfully!"
                                                                  delegate:nil
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
            [successAlert show];
       
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ) {
                [self dismissViewControllerAnimated:YES completion:nil];
            } else {
                self->deleted=YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTableView" object:nil];
            }
        
        } else {
            
            UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"CHANGE EVENT"
                                                                   message:@"Event was changed successfully!"
                                                                  delegate:nil
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
            [successAlert show];
    
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ) {
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                self->deleted=YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTableView" object:nil];
            }
        }
    }
}

-(IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)deleteEventInURL:(NSString *) eventIDTitle
{
    [[NetworkManager instance] GET:@"find/EventLogUser" parameters:nil
                        success:^(NSURLSessionDataTask * __unused task, id JSON) {
                            
                            NSArray *key = [JSON allKeys];
                            NSArray *object = [JSON allObjects];
                            
                            for (NSUInteger i=0; i<[key count]; i++)
                            {
                                NSDictionary *dict = [object objectAtIndex:i];
                                NSString *title = [dict valueForKey:TITLE];
                               
                                if ([title isEqualToString:eventIDTitle])
                                {
                                    NSString *deletedID =[[key objectAtIndex:i] description];
                                    
                                    [[NetworkManager instance] GET:[@"delete/" stringByAppendingString:deletedID] parameters:nil
                                                           success:^(NSURLSessionDataTask * __unused task, id JSON) {
                                                               
                                                               NSLog(@"Delete success %@",[JSON description]);
                                                               
                                                           }
                                                           failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
                                                               UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Delete event error!"
                                                                                                               message:[error description] delegate:nil
                                                                                                     cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                               [alert show];
                                                           }];
                                    
                                }
                            }
                            
                        }
                        failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
                            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Find event error!"
                                                                            message:[error description]
                                                                           delegate:nil
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil];
                            [alert show];
                        }];
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = @"Events";
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

#pragma mark - Show DetailView

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerForKeyboardNotification];
    [self refreshView];
}

-(void)refreshView {
    
    self.titleTextField.text = self.event.title;
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MM/dd/yyyy"];
    
    if ( self.event.date == nil ) {
        self.dateTextField.text = [format stringFromDate:[NSDate dateWithTimeIntervalSinceNow:3600]];
    } else {
        self.dateTextField.text = [format stringFromDate:self.event.date];
    }
    
    [format setDateFormat:@"h:mm a"];
    
    if ( self.event.time == nil ) {
        self.timeTextField.text = [format stringFromDate:[NSDate dateWithTimeIntervalSinceNow:3600]];
    } else {
        self.timeTextField.text = [format stringFromDate:self.event.time];
    }
    
    if ( self.event.recall == nil ) {
        self.recallTextField.text = [format stringFromDate:[NSDate dateWithTimeIntervalSinceNow:3000]];
    } else {
        self.recallTextField.text = [format stringFromDate:self.event.recall];
    }
    
    self.descTextField.text = self.event.descript;
    self.impotanceSegment.selectedSegmentIndex = [self.event.impotance boolValue] ? YES : NO;
    
    if (self.impotanceSegment.selectedSegmentIndex == YES) {
        UIColor *lightCyanColor = [UIColor colorWithRed:180.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1];
        self.titleTextField.backgroundColor = lightCyanColor;
        self.dateTextField.backgroundColor = lightCyanColor;
        self.timeTextField.backgroundColor = lightCyanColor;
        self.recallTextField.backgroundColor = lightCyanColor;
        self.descTextField.backgroundColor = lightCyanColor;
    } else {
        self.titleTextField.backgroundColor = [UIColor clearColor];
        self.dateTextField.backgroundColor = [UIColor clearColor];
        self.timeTextField.backgroundColor = [UIColor clearColor];
        self.recallTextField.backgroundColor = [UIColor clearColor];
        self.descTextField.backgroundColor = [UIColor clearColor];

    }
}

-(void)eventChanged:(Event *)newEvent {
    self.event=newEvent;
    [self refreshView];
    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Keyboard actions

-(void) registerForKeyboardNotification {
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardDidHideNotification object:nil];
}

-(void) keyboardWasShown:(NSNotification *)aNotification {
    
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets;
   
    CGFloat kbHeight;
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) ){
        contentInsets = UIEdgeInsetsMake(65, 0, kbSize.width, 0);
        kbHeight = kbSize.width;
    } else {
        contentInsets = UIEdgeInsetsMake(65, 0, kbSize.height, 0);
        kbHeight = kbSize.height;
    }
    
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbHeight;
    CGRect activeRect = activeField.frame;
    activeRect.origin.y+=activeField.frame.size.height/2;
    
    if (!CGRectContainsPoint(aRect, activeRect.origin) ) {
        activeRect = CGRectMake(0, activeRect.origin.y - kbHeight, 0, 100);
        [self.scrollView setContentOffset:activeRect.origin animated:YES];
    }
}

-(void) keyboardWillBeHidden:(NSNotification *)aNotification {
   
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(65, 0, 0, 0);
  
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

-(void) dismissKeyboard
{
    
    [self.titleTextField resignFirstResponder];
    [self.dateTextField resignFirstResponder];
    [self.timeTextField resignFirstResponder];
    [self.recallTextField resignFirstResponder];
    [self.descTextField resignFirstResponder];
}

-(IBAction)textFieldDoneEditing:(id)sender
{
    [sender resignFirstResponder];
}

#pragma mark - DataPicker Initialization

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    
    if ( textField == self.dateTextField || textField == self.timeTextField || textField == self.recallTextField )
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            UIViewController* popoverContent = [[UIViewController alloc] init]; //ViewController
            
            UIView *popoverView = [[UIView alloc] init];   //view
        
            
            CGRect pickerFrame = CGRectMake(0, 44, 320, 264);
            UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            
            if ( textField == self.dateTextField ) {
                dateORtime=0;
                [format setDateFormat:@"MM/dd/yyyy"];
                [datePicker setDatePickerMode:UIDatePickerModeDate];
                
            } else if ( textField == self.timeTextField ) {
                dateORtime=1;
                [format setDateFormat:@"h:mm a"];
                [datePicker setDatePickerMode:UIDatePickerModeTime];
            } else {
                dateORtime=2;
                [format setDateFormat:@"h:mm a"];
                [datePicker setDatePickerMode:UIDatePickerModeTime];
            }
            
            [datePicker setDate:[format dateFromString:textField.text ] animated:YES];
            
            [popoverView addSubview:datePicker];
            
            UIToolbar *controlToolbar = [[UIToolbar alloc]
                                         initWithFrame:CGRectMake(0, 0, 320, 44)];
            
            [controlToolbar setBarStyle:UIBarStyleBlack];
            
            UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
                                       UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
            UIBarButtonItem *cancelPicker = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:
                                             UIBarButtonItemStyleBordered target:self action:@selector(cancelDateSet_iPad)];
            UIBarButtonItem *donePicker = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:
                                           UIBarButtonItemStyleDone target:self action:@selector(doneDateSet_iPad)];
            
            [controlToolbar setItems:[NSArray arrayWithObjects:spacer,cancelPicker,donePicker, nil] animated:NO];
            
            [popoverView addSubview:controlToolbar];
   
            popoverContent.view = popoverView;
            UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverContent];

            popoverController.delegate=self;
           
            [popoverController setPopoverContentSize:CGSizeMake(320, 264) animated:NO];
            self.datePickerPopover = popoverController;
            
            CGRect aRect = textField.frame;
            aRect.origin.y+=65;
            [self.datePickerPopover presentPopoverFromRect:aRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
            NSArray *listOfViews = [popoverView subviews];
            [self.datePickerPopover setPassthroughViews:[NSArray arrayWithArray:listOfViews]];
            
            [textField resignFirstResponder];
            
        } else
        {
            dateSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:nil
                                           cancelButtonTitle:nil destructiveButtonTitle:nil
                                           otherButtonTitles:nil];
            [dateSheet setActionSheetStyle:UIActionSheetStyleDefault];
            
            CGRect rect = self.view.frame;
            
            CGRect pickerFrame = CGRectMake(0, 44, rect.size.width, 264);
            UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
            
            NSDateFormatter *format = [[NSDateFormatter alloc] init];
            
            if ( textField == self.dateTextField ) {
                dateORtime=0;
                [format setDateFormat:@"MM/dd/yyyy"];
                [datePicker setDatePickerMode:UIDatePickerModeDate];
            
            } else if ( textField == self.timeTextField ) {
                dateORtime=1;
                [format setDateFormat:@"h:mm a"];
                [datePicker setDatePickerMode:UIDatePickerModeTime];
            } else {
                dateORtime=2;
                [format setDateFormat:@"h:mm a"];
                [datePicker setDatePickerMode:UIDatePickerModeTime];
            }
            
            [datePicker setDate:[format dateFromString:textField.text ] animated:YES];
        
            [dateSheet addSubview:datePicker];
        
            UIToolbar *controlToolbar = [[UIToolbar alloc]
                                           initWithFrame:CGRectMake(0,0, dateSheet.bounds.size.width,44)];
    
            [controlToolbar setBarStyle:UIBarStyleBlack];
            [controlToolbar sizeToFit];
            
            UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
                                       UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
            UIBarButtonItem *cancelPicker = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:
                                        UIBarButtonItemStyleBordered target:self action:@selector(cancelDateSet_iPhone)];
            UIBarButtonItem *donePicker = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:
                                       UIBarButtonItemStyleDone target:self action:@selector(doneDateSet_iPhone)];
            
            [controlToolbar setItems:[NSArray arrayWithObjects:spacer,cancelPicker,donePicker, nil] animated:NO];
            
            [dateSheet addSubview:controlToolbar];
            
            [dateSheet showInView:self.view];
            
            if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) ){
                [dateSheet setBounds:CGRectMake(0, 0, rect.size.width, rect.size.height+rect.size.height/4)];
            } else {
                [dateSheet setBounds:CGRectMake(0, 0, rect.size.width, rect.size.height+rect.size.height/8)];
            }
            [textField resignFirstResponder];
        }
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField=nil;
}

-(void)cancelDateSet_iPhone
{
    [dateSheet dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)doneDateSet_iPhone
{
    
    NSArray *listOfViews = [dateSheet subviews];
    for (UIView *subView in listOfViews) {
            if ([subView isKindOfClass:[UIDatePicker class]]) {
                self.eventDate = [(UIDatePicker *)subView date];
            }
        }
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    
    if ( dateORtime == 0 ) {
        [format setDateFormat:@"MM/dd/yyyy"];
        [self.dateTextField setText:[format stringFromDate:self.eventDate]];
    } else if ( dateORtime == 1 ) {
        [format setDateFormat:@"h:mm a"];
        [self.timeTextField setText:[format stringFromDate:self.eventDate]];
    } else {
        [format setDateFormat:@"h:mm a"];
        [self.recallTextField setText:[format stringFromDate:self.eventDate]];
    }

    [dateSheet dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)cancelDateSet_iPad
{
    [self.datePickerPopover dismissPopoverAnimated:YES];
}

-(void)doneDateSet_iPad
{
    
    NSArray *listOfViews = [self.datePickerPopover passthroughViews] ;
    
    for (UIView *subView in listOfViews) {
        if ([subView isKindOfClass:[UIDatePicker class]]) {
            self.eventDate = [(UIDatePicker *)subView date];
        }
    }
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    
    if ( dateORtime == 0 ) {
        [format setDateFormat:@"MM/dd/yyyy"];
        [self.dateTextField setText:[format stringFromDate:self.eventDate]];
    } else if ( dateORtime == 1 ) {
        [format setDateFormat:@"h:mm a"];
        [self.timeTextField setText:[format stringFromDate:self.eventDate]];
    } else {
        [format setDateFormat:@"h:mm a"];
        [self.recallTextField setText:[format stringFromDate:self.eventDate]];
    }
    [self.datePickerPopover dismissPopoverAnimated:YES];
    
}

@end
