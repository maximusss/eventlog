//
//  DetailViewController.h
//  EventLog
//
//  Created by Mac on 11.11.13.
//  Copyright (c) 2013 maximusss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate, UIPopoverControllerDelegate> {

@private
    
    Event* _event;
    UITextField* _titleTextField;
    UITextField* _dateTextField;
    UITextField* _timeTextField;
    UITextField* _recallTextField;
    UITextField* _descTextField;
    UISegmentedControl* _impotanceSegment;
    UIScrollView* _scrollView;
    UIView* _contentView;
    UIPopoverController* _masterPopoverController;
    
    int dateORtime;                 //if 0 - dataTextField, 1 - timeTextField, 2 - recallTextField
    NSDate *_eventDate;
    UIActionSheet* dateSheet;
    
    IBOutlet UIButton *myButton;
@public

    BOOL deleted;
    BOOL modal;
}

@property (strong, nonatomic) id detailItem;


@property (nonatomic, retain) Event *event;
@property (nonatomic, retain) NSDate *eventDate;
@property (nonatomic, retain) IBOutlet UITextField *titleTextField;
@property (nonatomic, retain) IBOutlet UITextField *dateTextField;
@property (nonatomic, retain) IBOutlet UITextField *timeTextField;
@property (nonatomic, retain) IBOutlet UITextField *recallTextField;
@property (nonatomic, retain) IBOutlet UITextField *descTextField;
@property (nonatomic, retain) IBOutlet UISegmentedControl *impotanceSegment;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIView *contentView;

@property (nonatomic, weak) IBOutlet UILabel *detailDescriptionLabel;
@property (nonatomic, retain) UIPopoverController *datePickerPopover;
@property (strong, nonatomic) UIPopoverController *masterPopoverController;

-(IBAction)save:(id)sender;
-(IBAction)cancel:(id)sender;
-(IBAction)textFieldDoneEditing:(id)sender;
-(void)refreshView;
-(void)eventChanged:(Event *)newEvent;
-(void)deleteEventInURL:(NSString *) eventIDTitle;
-(void)registerForKeyboardNotification;
-(void)keyboardWasShown: (NSNotification *)aNotification;
-(void)keyboardWillBeHidden: (NSNotification *)aNotification;
-(void)dismissKeyboard;
-(void)doneDateSet_iPhone;
-(void)cancelDateSet_iPhone;
-(void)doneDateSet_iPad;
-(void)cancelDateSet_iPad;

@end
