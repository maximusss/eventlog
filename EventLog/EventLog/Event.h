//
//  Event.h
//  EventLog
//
//  Created by Mac on 20.11.13.
//  Copyright (c) 2013 maximusss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Event : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * descript;
@property (nonatomic, retain) NSNumber * impotance;
@property (nonatomic, retain) NSDate * recall;
@property (nonatomic, retain) NSDate * time;
@property (nonatomic, retain) NSString * title;

@end
