//
//  AddSegue.m
//  EventLog
//
//  Created by Mac on 29.11.13.
//  Copyright (c) 2013 maximusss. All rights reserved.
//

#import "AddSegue.h"
#import "DetailViewController.h"

@implementation AddSegue

-(void)perform
{
    DetailViewController *addVC = self.destinationViewController;
    addVC->deleted = FALSE;
    addVC->modal = TRUE;
    UINavigationController *addNC = [[UINavigationController alloc] initWithRootViewController:addVC];
    [self.sourceViewController presentViewController:addNC animated:YES completion:nil];
}
@end
